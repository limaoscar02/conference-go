import json

from django.http import JsonResponse
from .models import Presentation, Status
from events.api_views import Conference
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods


class PresentationListEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "title",
        # "status",
    ]

@require_http_methods(["GET", "POST"])
def api_list_presentations(request, conference_id):

    if request.method == "GET":
        presentation = Presentation.objects.get(conference_id=conference_id)
        status = Status.objects.all()
        return JsonResponse(
        {"presentation": presentation},
        encoder=PresentationListEncoder,
    )
    else:
        content = json.loads(request.body)

        try:
            conference = Conference.objects.get(id=conference_id)
            content["conference"] = conference

        except Conference.DoesNotExist:
            Presentation = Presentation.objects.create(**content)
            return JsonResponse(
                {"message": "Invaild conference id"},
            status=400,
        )
    return JsonResponse(
        presentation,
        encoder=PresentationDetailEncoder,
        safe=False,
    )



class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
    ]

def api_show_presentation(request, id):
    presentation = Presentation.objects.get(id=id)
    return JsonResponse(
        presentation,
        encoder=PresentationDetailEncoder,
        safe=False,
    )
