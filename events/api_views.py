from django.http import JsonResponse
from .models import Conference, Location, State
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json




class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = ["name"]

def api_list_conferences(request):
    """
    Lists the conference names and the link to the conference.

    Returns a dictionary with a single key "conferences" which
    is a list of conference names and URLS. Each entry in the list
    is a dictionary that contains the name of the conference and
    the link to the conference's information.

    {
        "conferences": [
            {
                "starts": the date/time when the conference starts,
                "ends": the date/time when the conference ends,
                "description": the description of the conference,
                "created": the date/time when the record was created,
                "updated": the date/time when the record was updated,
                "max_presentations": the maximum number of presentations,
                "max_attendees": the maximum number of attendees,
                "location":{"name": conference's name,
                "href": URL to the conference,
                }
            },
            ...
        ]
    }
    # """
    conferences = Conference.objects.all()
    return JsonResponse(
        {"conferences": conferences},
        encoder=ConferenceListEncoder,
    )


class LocationListEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        ]


class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location",
    ]
    encoders = {
        "location": LocationListEncoder(),
    }

def api_show_conference(request, id):
    conference = Conference.objects.get(id=id)
    return JsonResponse(
        conference,
        encoder=ConferenceDetailEncoder,
        safe=False,
    )
@require_http_methods(["GET","POST"])
def api_list_locations(request):
    """
    Lists the location names and the link to the location.

    Returns a dictionary with a single key "locations" which
    is a list of location names and URLS. Each entry in the list
    is a dictionary that contains the name of the location and
    the link to the location's information.

    {
        "locations": [
            {
                "name": location's name,
                "href": URL to the location,
            },
            ...
        ]
    }
    """
    if request.method == "GET":
        locations = Location.objects.all()
        return JsonResponse(
        {"locations": locations},
        encoder=LocationListEncoder,
    )
    else:
        content = json.loads(request.body)

        try:
            state = State.objects.get(abbreviation=content["state"])
            content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )

        location = Location.objects.create(**content)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )

# def api_list_locations(request):
#     # Lots of other code
#     else:
#         content = json.loads(request.body)

#         try:
#             state = State.objects.get(abbreviation=content["state"])
#             content["state"] = state
#         except State.DoesNotExist:
#             return JsonResponse(
#                 {"message": "Invalid state abbreviation"},
#                 status=400,
#             )

#         # Use the city and state's abbreviation in the content dictionary
#         # to call the get_photo ACL function

#         # Use the returned dictionary to update the content dictionary

#         location = Location.objects.create(**content)
#         return JsonResponse(
#             location,
#             encoder=LocationDetailEncoder,
#             safe=False,
#         )

class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
        ]

    def get_extra_data(self, o):
        return {"state": o.state.abbreviation }

@require_http_methods(["DELETE","GET","PUT"])
def api_show_location(request, id):


    if request.method == "GET":
        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )

    elif request.method == "DELETE":
        count, _ = Location.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
    # copied from create
        content = json.loads(request.body)
        try:
        # new code
            if "state" in content:
                state = State.objects.get(abbreviation=content["state"])
                content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
        )

    # new code
        Location.objects.filter(id=id).update(**content)

    # copied from get detail
        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
